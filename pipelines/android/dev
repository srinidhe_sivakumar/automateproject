#!groovy

/* 1. Static Analysis and Quality Gate check
 * 2. Build APK
 * 3. Archive APK
 * 4. Send the APK
 */
@Library('sharedlib') _

def buildProps = ""
def module = ""

pipeline {
              agent {
                  docker {
                      image 'circleci/android:api-28-alpha'
                      label 'iot-build'
                      alwaysPull true
                      args '-u root -v /var/run/docker.sock:/var/run/docker.sock -v /opt/docker/gradle:/root/.gradle'
                  }
              }
    parameters {
            choice(
            choices: ['Allow' , 'Skip'],
            description: '',
            name: 'REQUESTED_ACTION_SONARQUBE')
        }

    environment {
        DOCKER_REPO = 'nexus-versa.contus.us:8083'
        ENV = 'debug'
    }

    stages {
        stage('Static Analysis') {
          when {
           // This stage will run if a "Allow" is requested
           expression { params.REQUESTED_ACTION_SONARQUBE == 'Allow' }
          }
            steps {
               withSonarQubeEnv('contus_sonarqube') {
                 runSonarScanner sonarProperties: "sonar-project.properties"
                 sleep 60
                }
            }
        }
        stage('Quality Gate') {
          when {
           // This stage will run if a "Allow" is requested
           expression { params.REQUESTED_ACTION_SONARQUBE == 'Allow' }
          }
            steps {
                script {
                    def qg = waitForQualityGate()
                    if (qg.status != 'OK') {
                        // Send email and set the build status as failed

                        // Set Build Status
                        currentBuild.result = "FAILED"

                        // Read the build properties for developers email, and email template repo link
                        buildProps = readProperties file: 'build.properties'

                        // Send email to developers and ops team
                        // Send failure email with Sonarqube link attached
                        sendQGFailedEmail to: "${buildProps['build.email.dev']}, ${buildProps['build.email.ops']}"

                        //Set build status as failed with a message
                        error "Pipeline aborted due to quality gate failure: ${qg.status}"
                    } else {
                      // Set Build Status
                      currentBuild.result = "SUCCESS"

                      // Read the build properties for developers email, and email template repo link
                      buildProps = readProperties file: 'build.properties'

                      sendQGSuccessEmail to: "${buildProps['build.email.dev']}, ${buildProps['build.email.ops']}" }
                }
            }
        }
        stage('Build Dev') {
            steps {
                checkout scm
                script {
                    // Read the build properties
                    buildProps = readProperties file: 'build.properties'

                    // Build APK
                    buildAPK environment: "${ENV}", appName: "${buildProps['build.app.name']}"

                    // Set Build Status
                    currentBuild.result = "SUCCESS"

                    // Send deployment successful email
                    sendBuildSuccessEmail to: "${buildProps['build.email.dev']}, ${buildProps['build.email.ops']}", environment: "${ENV}"

                    //Archive Apk File
                    archiveArtifacts artifacts: '**/mymtb_*.apk'
                }
            }
        }
    }
    // The options directive is for configuration that applies to the whole job.
    options {
        skipDefaultCheckout()
        // We'd like to make sure we only keep 20 builds at a time, so
        // we don't fill up the storage!
        buildDiscarder(logRotator(numToKeepStr: '20'))

        // And we'd really like to be sure that this build doesn't hang forever, so
        // let's time it out after an hour.
        timeout(time: 60, unit: 'MINUTES')
    }
}
