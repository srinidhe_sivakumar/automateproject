#!/bin/bash

set -e
#set -x

echo "******* Create User *******"
echo "\n"
SSH_USER="srinidhi"
SSH_PASS="srinidhi123"
SSH_USER_HOME=${SSH_USER_HOME:-/home/${SSH_USER}}

# Create contus user (if not already present)
if ! id -u $SSH_USER >/dev/null 2>&1; then
	  echo "==> Creating $SSH_USER user"
	    /usr/sbin/groupadd $SSH_USER
	      /usr/sbin/useradd $SSH_USER -g $SSH_USER -G wheel -d $SSH_USER_HOME --create-home
	        echo "${SSH_USER}:${SSH_PASS}" | chpasswd
	fi

	# Set up sudo
	echo "==> Giving ${SSH_USER} sudo powers"
	echo "${SSH_USER}        ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers.d/contus
	chmod 440 /etc/sudoers.d/contus

	echo "\n"
	echo "******* Install Essential Packages *******"
	echo "\n"

	ESSENTIAL_PACKAGES="
	gawk
	nano
	curl
	tcpdump
	unzip
	wget
	"

	sudo apt-get update
	sudo apt-get install -y $ESSENTIAL_PACKAGES

	echo "\n"
	echo "******* Install Docker CE *******"
	echo "\n"

	echo "Install packages to allow apt to use a repository over HTTPS"
	sudo apt-get install \
		    apt-transport-https \
		        ca-certificates \
			    curl \
			        software-properties-common -y

	echo "Add Dockerâs official GPG key"
	sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

	echo "Add fingerprint"
	sudo apt-key fingerprint 0EBFCD88

	echo "Set up the stable repository"
	sudo add-apt-repository \
		   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
		      $(lsb_release -cs) \
		         stable"

	#Update again
	sudo apt-get update

	echo "Install latest docker"
	sudo apt-get install docker-ce -y

	echo "Add ${SSH_USER} to docker group"
	sudo usermod -aG docker ${SSH_USER}

