#!/bin/bash

set -e

## Setup Logs
echo "Rsyslog and Logrotate Setup"
sudo cp -r rsyslog/project.conf /etc/rsyslog.d/project.conf
sudo service rsyslog restart
sudo cp -r logrotate/projectlogs /etc/logrotate.d/projectlogs
