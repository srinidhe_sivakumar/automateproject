version: "3.7"

networks:
  go:
    name: go
    driver: overlay
    driver_opts:
      encrypted: "true"
  proxy:
    name: proxy
    driver: overlay
    driver_opts:
      encrypted: "true"

volumes:
  rabbitmq-data: {}
  rabbitmq-logs: {}
  mongodb: {}

configs:
  rabbitmq-enable-plugins:
    file: ./configs/enable_plugins
  rabbitmq-conf:
    file: ./configs/rabbitmq.conf
  api-env-6:
    file: ./configs/api.env
  openapi-env:
    file: ./configs/openapi.json
  dashboard-env-2:
    file: ./configs/dashboard.env
  haproxy-env:
    file: ./configs/haproxy.cfg

services:
  haproxy:
    image: haproxy
    networks:
      - proxy
    ports:
      - target: 80
        published: 8080
        protocol: tcp
        mode: host
      - target: 443
        published: 8443
        protocol: tcp
        mode: host
      - target: 1936
        published: 1936
        protocol: tcp
    configs:
      - source: haproxy-env
        target: /usr/local/etc/haproxy/haproxy.cfg
    logging:
      driver: syslog
      options:
        syslog-address: "udp://127.0.0.1:514"
        tag: "haproxy/{{.ImageName}}/{{.ID}}"
        syslog-facility: local0
        syslog-format: rfc5424micro
    command: /bin/sh -c "/sbin/syslogd -O /proc/1/fd/1 && haproxy -f /usr/local/etc/haproxy/haproxy.cfg"
    deploy:
      mode: global

  dashboard:
    image: project_dashboard_dev:1
    networks:
      - proxy
      - go
    configs:
      - source: dashboard-env-2
        target: /var/www/html/assets/configuration/configuration.json
    logging:
      driver: syslog
      options:
        syslog-address: "udp://127.0.0.1:514"
        tag: "dashboard/{{.ImageName}}/{{.ID}}"
        syslog-facility: local1
        syslog-format: rfc5424micro
    healthcheck:
      disable: true
    deploy:
      mode: replicated
      replicas: 1

  api:
    image: project_api_dev:83
    networks:
      - proxy
      - go
    logging:
      driver: syslog
      options:
        syslog-address: "udp://127.0.0.1:514"
        tag: "api/{{.ImageName}}/{{.ID}}"
        syslog-facility: local2
        syslog-format: rfc5424micro
    healthcheck:
      disable: true
    configs:
      - source: api-env-6
        target: /home/.env
      - source: openapi-env
        target: /home/openapi.json
    deploy:
      mode: replicated
      replicas: 1

  rabbitmq:
    image: rabbitmq:3.7.14
    hostname: my-rabbitmq
    networks:
      - go
    ports:
      - target: 15672
        published: 9000
        protocol: tcp
    environment:
      RABBITMQ_DEFAULT_USER: admin
      RABBITMQ_DEFAULT_PASS: admin123
      RABBITMQ_CONFIG_FILE: /etc/rabbitmq/rabbitmq1.conf
    volumes:
      - rabbitmq-data:/var/lib/rabbitmq
      - rabbitmq-logs:/var/log/rabbitmq
    configs:
      - source: rabbitmq-enable-plugins
        target: /etc/rabbitmq/enabled_plugins
      - source: rabbitmq-conf
        target: /etc/rabbitmq/rabbitmq1.conf
    logging:
      driver: syslog
      options:
        syslog-address: "udp://127.0.0.1:514"
        tag: "rabbitmq/{{.ImageName}}/{{.ID}}"
        syslog-facility: local3
        syslog-format: rfc5424micro

  mongodb:
    image: mongodb:4.0
    networks:
      - go
    ports:
      - target: 27017
        published: 27017
        protocol: tcp
    volumes:
      - mongodb:/data/db
    logging:
      driver: syslog
      options:
        syslog-address: "udp://127.0.0.1:514"
        tag: "mongodb/{{.ImageName}}/{{.ID}}"
        syslog-facility: local4
        syslog-format: rfc5424micro
    command: mongod --keyFile /data/key --bind_ip 0.0.0.0
    deploy:
      mode: replicated
      replicas: 1
